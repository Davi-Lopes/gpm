# GIMP Profile Manager
![GPM logo](assets/gpm.png)  
# A nice tool for managing GIMP profiles 

![Gimp profile manager](assets/example2.png)

# OUTDATED!
## Pleas, go to [https://codeberg.org/frutinha/gimp_profile_manager](https://codeberg.org/frutinha/gimp_profile_manager).

Install:

```
git clone https://gitlab.com/Davi-Lopes/gpm.git
cd gpm
./install.py
```

Uninstall:

```
rm -rf ~/.GPM
```

### Usage

You can list all profiles you have  
```bash
$ gprofiler --list
Default *
PixelArt
Illumination
```
("*" represents the selected profile)

***

gpm + profile name to easily swicth between profiles  
```bash
$ gprofiler PixelArt
$ gprofiler --list
Default
PixelArt *
Illumination
```  

***

Create a new profile by using:  
```bash
$ gprofiler --create Photography
```  

***

Remove unused profiles  
```bash
$ gprofiler --remove Illumination
```
