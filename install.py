#!/usr/bin/env python

import os
from pathlib import Path

print("""

  $$$$$$\\                                 $$$$$$\\  $$\\ $$\\                     
$$  __$$\\                               $$  __$$\\ \\__|$$ |                    
$$ /  \\__| $$$$$$\\   $$$$$$\\   $$$$$$\\  $$ /  \\__|$$\\ $$ | $$$$$$\\   $$$$$$\\  
$$ |$$$$\\ $$  __$$\\ $$  __$$\\ $$  __$$\\ $$$$\\     $$ |$$ |$$  __$$\\ $$  __$$\\ 
$$ |\\_$$ |$$ /  $$ |$$ |  \\__|$$ /  $$ |$$  _|    $$ |$$ |$$$$$$$$ |$$ |  \\__|
$$ |  $$ |$$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |$$ |$$   ____|$$ |      
\\$$$$$$  |$$$$$$$  |$$ |      \\$$$$$$  |$$ |      $$ |$$ |\\$$$$$$$\\ $$ |      
\\______/ $$  ____/ \\__|       \\______/ \\__|      \\__|\\__| \\_______|\\__|      
          $$ |                                                                
          $$ |                                                                
          \\__|

Made with love by Davi!
""")
# Detect GIMP directory
USER_DIR = str(Path.home())
GPROFILER_PATH = USER_DIR + '/.local/bin/gprofiler'
FLATPAK = f'{USER_DIR}/.var/app/org.gimp.GIMP/config/GIMP/2.10'
DEFAULT = f'{USER_DIR}/.config/GIMP/2.10'

if os.path.isdir(GPROFILER_PATH):
    print('GPM Already installed!')
else:
    print('Creating directories --- <3')
    os.makedirs(GPROFILER_PATH)
    os.makedirs(GPROFILER_PATH + '/profiles/Default/2.10')

    os.makedirs(GPROFILER_PATH + '/bin')
    os.system(f'git clone https://gitlab.com/Davi-Lopes/gpm.git {GPROFILER_PATH}/bin')

# Create config file
CURRENT_PROFILE_FILE_PATH = GPROFILER_PATH + '/current_profile'
GIMP_PATH_FILE_PATH = GPROFILER_PATH + '/gimp_path'
Path(CURRENT_PROFILE_FILE_PATH).touch()
Path(GIMP_PATH_FILE_PATH).touch()

with open(CURRENT_PROFILE_FILE_PATH, 'w') as f:
    f.write("Default")

    with open(GIMP_PATH_FILE_PATH, 'w') as f:
        if os.path.isdir(FLATPAK):
            f.write(FLATPAK)
        elif os.path.isdir(DEFAULT):
            f.write(DEFAULT)
        else:
            print('Cant found gimp path, please provide it in gimp path file')


Path(GPROFILER_PATH + '/profiles/Default').mkdir(parents=True, exist_ok=True)

print('')
print(' ######################################################################## ')
print("## >->->--------------------------< 00 >--------------------------<-<-< ##")
print('# Installaton finished! please, add "~/.local/bin/gprofiler/bin" to PATH #')
print("## >->->--------------------------< 00 >--------------------------<-<-< ##")
print(' ######################################################################## ')
